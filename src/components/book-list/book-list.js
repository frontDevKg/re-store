import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { booksLoaded, booksRequested } from '../../reducers/actions/index';
import BookListItem from '../book-list-item';
import { withBookstoreService } from '../hoc';
import Spinner from '../spinner';
import './book-list.css';

const BookList = ({
  books,
  loading,
  booksRequested,
  bookstoreService,
  booksLoaded,
}) => {
  useEffect(() => {
    booksRequested();
    console.log('render component blist');
    bookstoreService.getBooks().then((data) => booksLoaded(data));
  }, []);

  if (loading) {
    return <Spinner />;
  }

  return (
    <ul className="book-list">
      {books.map((book) => {
        return (
          <li key={book.id}>
            <BookListItem book={book} />
          </li>
        );
      })}
    </ul>
  );
};

const mapStateToProps = ({ books, loading }) => {
  return { books, loading };
};

const mapDispatchToProps = {
  booksLoaded,
  booksRequested,
};

export default withBookstoreService(
  connect(mapStateToProps, mapDispatchToProps)(BookList)
);
