[33mcommit 0b9204aa813e8dec60dffeeda6db9c8761295027[m[33m ([m[1;36mHEAD -> [m[1;32mdev[m[33m, [m[1;31morigin/master[m[33m, [m[1;31morigin/HEAD[m[33m, [m[1;32mmaster[m[33m)[m
Author: Abdumalik Kochkorov <theabdyko@gmail.com>
Date:   Sun Nov 14 18:55:21 2021 +0600

    Lesson 11

[1mdiff --git a/public/favicon.ico b/public/favicon.ico[m
[1mdeleted file mode 100644[m
[1mindex a11777c..0000000[m
Binary files a/public/favicon.ico and /dev/null differ
[1mdiff --git a/public/index.html b/public/index.html[m
[1mold mode 100644[m
[1mnew mode 100755[m
[1mindex aa069f2..cd80a85[m
[1m--- a/public/index.html[m
[1m+++ b/public/index.html[m
[36m@@ -2,42 +2,16 @@[m
 <html lang="en">[m
   <head>[m
     <meta charset="utf-8" />[m
[31m-    <link rel="icon" href="%PUBLIC_URL%/favicon.ico" />[m
[31m-    <meta name="viewport" content="width=device-width, initial-scale=1" />[m
[31m-    <meta name="theme-color" content="#000000" />[m
     <meta[m
[31m-      name="description"[m
[31m-      content="Web site created using create-react-app"[m
[32m+[m[32m      name="viewport"[m
[32m+[m[32m      content="width=device-width, initial-scale=1, shrink-to-fit=no"[m
     />[m
[31m-    <link rel="apple-touch-icon" href="%PUBLIC_URL%/logo192.png" />[m
[31m-    <!--[m
[31m-      manifest.json provides metadata used when your web app is installed on a[m
[31m-      user's mobile device or desktop. See https://developers.google.com/web/fundamentals/web-app-manifest/[m
[31m-    -->[m
[31m-    <link rel="manifest" href="%PUBLIC_URL%/manifest.json" />[m
[31m-    <!--[m
[31m-      Notice the use of %PUBLIC_URL% in the tags above.[m
[31m-      It will be replaced with the URL of the `public` folder during the build.[m
[31m-      Only files inside the `public` folder can be referenced from the HTML.[m
[31m-[m
[31m-      Unlike "/favicon.ico" or "favicon.ico", "%PUBLIC_URL%/favicon.ico" will[m
[31m-      work correctly both with client-side routing and a non-root public URL.[m
[31m-      Learn how to configure a non-root public URL by running `npm run build`.[m
[31m-    -->[m
[31m-    <title>React App</title>[m
[32m+[m[32m    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" />[m
[32m+[m[32m    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />[m
[32m+[m[32m    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700" rel="stylesheet">[m
[32m+[m[32m    <title>ReStore</title>[m
   </head>[m
   <body>[m
[31m-    <noscript>You need to enable JavaScript to run this app.</noscript>[m
     <div id="root"></div>[m
[31m-    <!--[m
[31m-      This HTML file is a template.[m
[31m-      If you open it directly in the browser, you will see an empty page.[m
[31m-[m
[31m-      You can add webfonts, meta tags, or analytics to this file.[m
[31m-      The build step will place the bundled scripts into the <body> tag.[m
[31m-[m
[31m-      To begin the development, run `npm start` or `yarn start`.[m
[31m-      To create a production bundle, use `npm run build` or `yarn build`.[m
[31m-    -->[m
   </body>[m
 </html>[m
[1mdiff --git a/public/logo192.png b/public/logo192.png[m
[1mdeleted file mode 100644[m
[1mindex fc44b0a..0000000[m
Binary files a/public/logo192.png and /dev/null differ
[1mdiff --git a/public/logo512.png b/public/logo512.png[m
[1mdeleted file mode 100644[m
[1mindex a4e47a6..0000000[m
Binary files a/public/logo512.png and /dev/null differ
[1mdiff --git a/public/manifest.json b/public/manifest.json[m
[1mdeleted file mode 100644[m
[1mindex 080d6c7..0000000[m
[1m--- a/public/manifest.json[m
[1m+++ /dev/null[m
[36m@@ -1,25 +0,0 @@[m
[31m-{[m
[31m-  "short_name": "React App",[m
[31m-  "name": "Create React App Sample",[m
[31m-  "icons": [[m
[31m-    {[m
[31m-      "src": "favicon.ico",[m
[31m-      "sizes": "64x64 32x32 24x24 16x16",[m
[31m-      "type": "image/x-icon"[m
[31m-    },[m
[31m-    {[m
[31m-      "src": "logo192.png",[m
[31m-      "type": "image/png",[m
[31m-      "sizes": "192x192"[m
[31m-    },[m
[31m-    {[m
[31m-      "src": "logo512.png",[m
[31m-      "type": "image/png",[m
[31m-      "sizes": "512x512"[m
[31m-    }[m
[31m-  ],[m
[31m-  "start_url": ".",[m
[31m-  "display": "standalone",[m
[31m-  "theme_color": "#000000",[m
[31m-  "background_color": "#ffffff"[m
[31m-}[m
[1mdiff --git a/public/robots.txt b/public/robots.txt[m
[1mdeleted file mode 100644[m
[1mindex e9e57dc..0000000[m
[1m--- a/public/robots.txt[m
[1m+++ /dev/null[m
[36m@@ -1,3 +0,0 @@[m
[31m-# https://www.robotstxt.org/robotstxt.html[m
[31m-User-agent: *[m
[31m-Disallow:[m
[1mdiff --git a/src/components/book-list/book-list.js b/src/components/book-list/book-list.js[m
[1mindex 19e8e31..3d1aa36 100644[m
[1m--- a/src/components/book-list/book-list.js[m
[1m+++ b/src/components/book-list/book-list.js[m
[36m@@ -1,4 +1,4 @@[m
[31m-import React, { Component } from 'react';[m
[32m+[m[32mimport React, { useEffect } from 'react';[m
 import BookListItem from '../book-list-item';[m
 import { connect } from 'react-redux';[m
 [m
[36m@@ -8,34 +8,27 @@[m [mimport { compose } from '../../utils';[m
 import Spinner from '../spinner';[m
 import './book-list.css';[m
 [m
[31m-class BookList extends Component {[m
[32m+[m[32mconst BookList = ({books, loading, bookstoreService, booksLoaded}) => {[m
[32m+[m[32m  useEffect(() => {[m
[32m+[m[32m    bookstoreService.getBooks().then((data) => booksLoaded(data))[m
[32m+[m[32m  })[m
 [m
[31m-  componentDidMount() {[m
[31m-[m
[31m-    const { bookstoreService, booksLoaded } = this.props;[m
[31m-    bookstoreService.getBooks()[m
[31m-      .then((data) => booksLoaded(data));[m
[32m+[m[32m  if (loading) {[m
[32m+[m[32m    return <Spinner />;[m
   }[m
 [m
[31m-  render() {[m
[31m-    const { books, loading } = this.props;[m
[31m-[m
[31m-    if (loading) {[m
[31m-      return <Spinner />;[m
[31m-    }[m
[31m-[m
[31m-    return ([m
[31m-      <ul className="book-list">[m
[31m-        {[m
[31m-          books.map((book) => {[m
[31m-            return ([m
[31m-              <li key={book.id}><BookListItem book={book}/></li>[m
[31m-            )[m
[31m-          })[m
[31m-        }[m
[31m-      </ul>[m
[31m-    );[m
[31m-  }[m
[32m+[m[32m  return ([m
[32m+[m[32m    <ul className="book-list">[m
[32m+[m[32m      {[m
[32m+[m[32m        books.map((book) => {[m
[32m+[m[32m          return ([m
[32m+[m[32m            <li key={book.id}><BookListItem book={book}/></li>[m
[32m+[m[32m          )[m
[32m+[m[32m        })[m
[32m+[m[32m      }[m
[32m+[m[32m    </ul>[m
[32m+[m[32m  );[m
[32m+[m
 }[m
 [m
 const mapStateToProps = ({ books, loading }) => {[m
[36m@@ -46,7 +39,9 @@[m [mconst mapDispatchToProps = {[m
   booksLoaded[m
 };[m
 [m
[31m-export default compose([m
[31m-  withBookstoreService(),[m
[31m-  connect(mapStateToProps, mapDispatchToProps)[m
[31m-)(BookList);[m
[32m+[m[32mexport default withBookstoreService([m
[32m+[m[32m  connect([m
[32m+[m[32m    mapStateToProps,[m
[32m+[m[32m    mapDispatchToProps[m
[32m+[m[32m  )(BookList)[m
[32m+[m[32m);[m
[1mdiff --git a/src/components/hoc/with-bookstore-service.js b/src/components/hoc/with-bookstore-service.js[m
[1mindex 6fad115..64b3be4 100644[m
[1m--- a/src/components/hoc/with-bookstore-service.js[m
[1m+++ b/src/components/hoc/with-bookstore-service.js[m
[36m@@ -1,7 +1,7 @@[m
 import React from 'react';[m
 import { BookstoreServiceConsumer } from '../bookstore-service-context';[m
 [m
[31m-const withBookstoreService = () => (Wrapped) => {[m
[32m+[m[32mconst withBookstoreService = (Wrapped) => {[m
 [m
   return (props) => {[m
     return ([m
[1mdiff --git a/src/reducers/index.js b/src/reducers/index.js[m
[1mindex c1b34bf..0299bb0 100644[m
[1m--- a/src/reducers/index.js[m
[1m+++ b/src/reducers/index.js[m
[36m@@ -1,4 +1,3 @@[m
[31m-[m
 const initialState = {[m
   books: [],[m
   loading: true[m
[1mdiff --git a/src/utils/compose.js b/src/utils/compose.js[m
[1mdeleted file mode 100644[m
[1mindex 7345cf7..0000000[m
[1m--- a/src/utils/compose.js[m
[1m+++ /dev/null[m
[36m@@ -1,6 +0,0 @@[m
[31m-const compose = (...funcs) => (comp) => {[m
[31m-  return funcs.reduceRight([m
[31m-    (wrapped, f) => f(wrapped), comp);[m
[31m-};[m
[31m-[m
[31m-export default compose;[m
[1mdiff --git a/src/utils/index.js b/src/utils/index.js[m
[1mdeleted file mode 100644[m
[1mindex ba1951f..0000000[m
[1m--- a/src/utils/index.js[m
[1m+++ /dev/null[m
[36m@@ -1,5 +0,0 @@[m
[31m-import compose from './compose';[m
[31m-[m
[31m-export {[m
[31m-  compose[m
[31m-};[m
